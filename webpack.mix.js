const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .js('resources/assets/admin/js/main-buid.js','public/js/admin-main.js')
   .sass('resources/assets/admin/admin-app.scss', 'public/css')
   .scripts([
    	'public/plugins/slimScroll/jquery.slimscroll.min.js',
    	'public/plugins/fastclick/fastclick.js',
    	'public/plugins/iCheck/icheck.min.js',
    	'resources/assets/admin/js/app.min.js'
    ],'public/js/admin-app.js');
