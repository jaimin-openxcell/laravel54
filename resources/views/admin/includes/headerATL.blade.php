<header class="main-header">
  <!-- Logo -->
  <a href="{{ url('/admin') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>{{ substr(config('app.name'),0,1) }}</b>{{ ucwords(substr(config('app.name'),4,1)) }}</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>{{ substr(config('app.name'),0,3) }}</b>{{ substr(config('app.name'),3) }}</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>
    <!-- <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">APIs <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="{{url('/admin/apidoc/create') }}">Add API</a></li>
          <li><a href="{{url('/admin/apidoc') }}">Api List</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="{{url('/admin/apicategory') }}">Api Category</a></li>
        </ul>
      </li>
    </ul> -->

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ Auth::user()->pic!=''?asset('storage/'.Auth::user()->pic):asset('storage/'.Auth::user()->pic).'/avatar.png' }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ ucwords(Auth::user()->name) }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{ Auth::user()->pic!=''?asset('storage/'.Auth::user()->pic):asset('storage/'.Auth::user()->pic).'/avatar.png' }}" class="img-circle" alt="User Image">
              <p>
                {{ ucwords(Auth::user()->name) }}
                <small>Member since Nov. 2012</small>
              </p>
            </li>
            <!-- Menu Body -->
            <li class="user-body">
              <!-- <div class="row">
                <div class="col-xs-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </div> -->
              <!-- /.row -->
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{ url('/admin/profile') }}" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="{{ url('/admin/logout') }}" 
                  onclick="event.preventDefault();document.getElementById('logout-form').submit();" 
                  class="btn btn-default btn-flat">Sign out</a>
                  <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>