<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <!-- <div class="user-panel">
      <div class="pull-left image">
        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ ucwords(Auth::user()->name) }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div> -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      
      <li class="{{ $current_route_name=='admin.home'?'active':'' }}">
        <a href="{{ url('/admin') }}">
          <i class="fa fa-th"></i> <span>Dashboard</span>
        </a>
      </li>
      @if(auth()->user()->can('developerOnly') || auth()->user()->can('role') || auth()->user()->can('admin.list'))
        <li class="header">ADMINISTRATING</li>
      @endif
      {{-- <li>{{$current_route_name}}</li> --}}
      @if(auth()->user()->can('developerOnly') || auth()->user()->can('role'))
      <li class="treeview {{ in_array($current_route_name,['admin.roles.index','admin.permissions.index','admin.myrolepermission'])?'active':'' }}">
        <a href="javascript:void(0);">
          <i class="fa fa-shield"></i> <span>Role Manager</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>

        <ul class="treeview-menu">
          <li class="{{ $current_route_name=='admin.roles.index'?'active':'' }}">
            <a href="{{ url('/admin/roles') }}"><i class="fa fa-circle-o"></i> Roles</a>
          </li>
          @can('developerOnly')
          <li class="{{ $current_route_name=='admin.permissions.index'?'active':'' }}">
            <a href="{{ url('/admin/permissions') }}"><i class="fa fa-circle-o"></i> Permissions</a>
          </li>
          @endcan
          <li class="{{ $current_route_name=='admin.myrolepermission'?'active':'' }}">
            <a href="{{ url('/admin/rolePermissions') }}"><i class="fa fa-circle-o"></i> Role Permissions</a>
          </li>
        </ul>
      </li>
      @endif
      
      @if(auth()->user()->can('developerOnly') || auth()->user()->can('admin.list'))
      <li class="{{ $current_route_name=='admin.administrator.index'?'active':'' }}">
        <a href="{{ url('/admin/administrator') }}">
          <i class="fa  fa-user-secret"></i> <span>Admin Users</span>
          <span class="pull-right-container">
            {{-- <small class="label pull-right bg-green">Admins</small> --}}
          </span>
        </a>
      </li>
      @endif
      
      <li class="header">Users</li>
      {{-- USER MANAGER --}}
      <li class="treeview {{ in_array($current_route_name,['admin.user.index','admin.user.create','admin.user.edit'])?'active':'' }}">
        <a href="javascript:void(0);">
          <i class="fa fa-users"></i> <span>Users Management</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>

        <ul class="treeview-menu">
          @if(auth()->user()->can('developerOnly') || auth()->user()->can('user.list'))
            <li class="{{ $current_route_name=='admin.user.index'?'active':'' }}">
              <a href="{{ url('/admin/user') }}"><i class="fa fa-list"></i> List</a>
            </li>
          @endif

          @if(auth()->user()->can('developerOnly') || auth()->user()->can('user.create'))
            <li class="{{ $current_route_name=='admin.user.create'?'active':'' }}">
              <a href="{{ url('/admin/user/create') }}"><i class="fa fa-plus-square-o"></i> Create User</a>
            </li>
          @endif
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>