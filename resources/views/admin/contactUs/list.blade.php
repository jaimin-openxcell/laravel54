@extends('admin.layout.master')
@section('content')
	<section class="content-header">
		<h1>
			Contact Request
			<small>Requests</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Contact Request</li>
		</ol>
	</section>

	<section class="content" id="app">
		@include('flash')
        <contact-requests headline='User'></contact-requests>
	</section>
@endsection
