@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <h1>
            User Add
            <small>Manage Users</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('/admin/user') }}">Users</a></li>
            <li class="active">Add User</li>
        </ol>
    </section>
    <section class="content" id="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add API</h3>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/user') }}" enctype="multipart/form-data">
                        <div class="box-body">
                            {{ csrf_field() }}
                            @include('flash')
                            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="col-md-3 control-label">First Name</label>

                                <div class="col-md-7">
                                    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" autofocus>

                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Last Name --}}
                            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="col-md-3 control-label">Last Name</label>

                                <div class="col-md-7">
                                    <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" autofocus>

                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Email --}}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-3 control-label">Email</label>

                                <div class="col-md-7">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- PIC --}}
                            <div class="form-group{{ $errors->has('pic') ? ' has-error' : '' }}">
                                    <label for="pic" class="col-md-3 control-label">Picture</label>

                                    <div class="col-md-7">
                                        <img-fileinput imgsrc="{{ $defaultImg }}"></img-fileinput>
                                        @if ($errors->has('pic'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('pic') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clear-fix"></div>
                            <div class="form-group">
                                <div class="col-md-7 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                    <a type="button" href="{{ url('/admin/user') }}" class="btn btn-info">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
