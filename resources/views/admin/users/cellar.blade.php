@extends('admin.layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('/plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            {{ ucwords($user->first) }}'s Cellar
            <small>Manager User Cellar</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Cellar</li>
        </ol>
    </section>

    <section class="content" id="app">
        @include('flash')
        <user-cellar headline="{{ ucwords($user->first) }}'s cellar" usr='{{ $user->api_token }}'></user-cellar>
    </section>
@endsection