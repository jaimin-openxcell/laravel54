@extends('admin.layout.master')

@section('content')
    <section class="content-header">
        <h1>
            User Edit
            <small>Manage Users</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('/admin/user') }}">Users</a></li>
            <li class="active">Edit User</li>
        </ol>
    </section>
    <section class="content" id="app">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add User</h3>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/user/'.$user->id) }}" enctype="multipart/form-data">
                        <div class="box-body">
                            {{ csrf_field() }}
                        	{{ method_field('PUT') }}
                            @include('flash')
                            <div class="form-group{{ $errors->has('first') ? ' has-error' : '' }}">
                                <label for="first" class="col-md-3 control-label">First Name</label>

                                <div class="col-md-7">
                                    <input id="first" type="text" class="form-control" name="first" value="{{ $user->first }}" autofocus>

                                    @if ($errors->has('first'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Last Name --}}
                            <div class="form-group{{ $errors->has('last') ? ' has-error' : '' }}">
                                <label for="last" class="col-md-3 control-label">Last Name</label>

                                <div class="col-md-7">
                                    <input id="last" type="text" class="form-control" name="last" value="{{ $user->last }}" autofocus>

                                    @if ($errors->has('last'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- Email --}}
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-3 control-label">Email</label>

                                <div class="col-md-7">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            {{-- PIC --}}
                            <div class="form-group{{ $errors->has('pic') ? ' has-error' : '' }}">
                                    <label for="pic" class="col-md-3 control-label">Picture</label>

                                    <div class="col-md-7">
                                        <img-fileinput imgsrc="{{$picture}}"></img-fileinput>
                                        @if ($errors->has('pic'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('pic') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="clear-fix"></div>
                            <div class="form-group">
                                <div class="col-md-7 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                    <a type="button" href="{{ url('/admin/user') }}" class="btn btn-info">
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
