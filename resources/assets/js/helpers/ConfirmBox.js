export default class ConfirmBox{
	constructor($this){
		this.ref=$this;
	}
	removeBox(slug,text, obj){
		let self=this.ref;
		swal({
          title: "Are you sure?",
          text: text,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
            self.remove(obj);
            swal("Deleted!", `Your ${slug} has been deleted.`, "success");
          } else {
            swal("Cancelled", `Your ${slug} is safe :)`, "error");
          }
        });
	}
  bulkRemoveBox(slug,text){
    let self=this.ref;
    swal({
      title: "Are you sure?",
      text: text,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete all!",
      cancelButtonText: "No, cancel!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        self.removeMultiple();
        swal("Deleted!", `Your selected ${slug} has been deleted.`, "success");
      } else {
        swal("Cancelled", `Your selected ${slug} is safe :)`, "error");
      }
    });
  }

  bulkStatusBox(slug,text){
    let self=this.ref;
    swal({
      title: "Are you sure?",
      text: text,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, update all!",
      cancelButtonText: "No, cancel!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        self.switchStatusSelected();
        swal("Deleted!", `Your selected ${slug} has been updated.`, "success");
      } else {
        swal("Cancelled", `Your selected ${slug} are not updated :)`, "error");
      }
    });
  }

  rejectConfirm(slug,text,obj, type){
    let self=this.ref;
    swal({
      title: "Are you sure?",
      text: text,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, move to rejection list!",
      cancelButtonText: "No, cancel!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        self.reject(obj, type);
        swal("Deleted!", `${slug} has been moved to rejected list.`, "success");
      } else {
        swal("Cancelled", `${slug} is safe :)`, "error");
      }
    });
  }
  assignConfirm(slug,text,obj,year='Non-Vintage'){
    let self=this.ref;
    swal({
      title: "Are you sure?",
      text: text,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: `Yes, Assign it with ${year} year!`,
      cancelButtonText: "No, cancel!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {
        self.assign(obj, year);
        swal("Approved!", `${slug} has been approved.`, "success");
      } else {
        swal("Cancelled", `${slug} is still in pending queue :)`, "error");
      }
    });
  }
}