<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'not-found'                  => 'Not found!',
    "unauthorized-access"        =>"Denied! Unauthorized Access!",
    "parameters-fail-validation" =>"Fail to pass validation process.",
    
    // ROLE
    "role-add"     =>"New role (:rolename) added successfully.",
    "role-update"  =>"Role updated successfully.",
    "role-distroy" =>"Role deleted successfully.",

    // ADMIN
    "admin-add"     =>"New admin added successfully.",
    "admin-update"  =>"Admin updated successfully.",
    "admin-distroy" =>"Admin deleted successfully.",
    "admin-status"  =>"Status has been changed to :status",
    "admin-profile-update"=>"Your profile has been updated!",

    // PERMISSION
    "permission-add"            =>"New permission added successfully.",
    "permission-update"         =>"Permission updated successfully.",
    "permission-distroy"        =>"Permission deleted successfully.",

    // USER
    "user-add"     =>"New user added successfully.",
    "user-update"  =>"User updated successfully.",
    "user-distroy" =>"User deleted successfully.",
    "user-status"  =>"Status has been changed to :status",
    "user-update-fail"=>"Fail to update user information.",
    "user-not-found"=>"No user found with this ID",
];
