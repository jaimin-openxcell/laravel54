<?php 
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class  ApiController extends Controller{
	
	protected $statuscode=200;
	protected $auth_token;
	public function __construct()
	{
		/*$res            =Request::instance();
		$REQUEST_URI    =$res->server('REQUEST_URI');
		$REMOTE_ADDR    =$res->server('REMOTE_ADDR');
		$REQUEST_METHOD =$res->server('REQUEST_METHOD');
		$HTTP_AUTHKEY   =$res->server('HTTP_AUTHKEY');
		$INPUT          = $res->input();
		\Logapp::create(["ip"=>$REMOTE_ADDR,"api_called"=>$REQUEST_URI,"request_type"=>$REQUEST_METHOD,"authKey"=>$HTTP_AUTHKEY,"request"=>$INPUT]);*/
	}
	protected function getToken(){
        return $this->auth_token;
    }
	public function setToken(){
        $this->auth_token=request()->get('api_token');
		return $this;	
	}
    protected function authorized(){
    	return \Auth::guard('api')->user();
    }
	public function getStatuscode(){
		return $this->statuscode;
	}
	public function setStatuscode($statuscode){
		$this->statuscode=$statuscode;
		return $this;	
	}
	

	public function respondNotFound($message = ""){
		$message=($message=='')?trans('messages.not-found'):$message;
		return $this->setStatuscode(404)->respondWithError($message);
	}	
	public function respondInternalError($message = ""){
		$message=($message=='')?trans('messages.internal-error'):$message;
		return $this->setStatuscode(500)->respondWithError($message);
	}
	public function respondAuthError($message = ""){
		$message=($message=='')?trans('messages.unauthorized-access'):$message;
		return $this->setStatuscode(401)->respondWithError($message);
	}
	public function respondAuthRoleError($message = ""){
		$message=($message=='')?trans('messages.unauthorized-access-role'):$message;
		return $this->setStatuscode(501)->respondWithError($message);
	}
	public function respondStatusError($message = ""){
		$message=($message=='')?trans('messages.account-inactive'):$message;
		return $this->setStatuscode(403)->respondWithError($message);
	}
	public function respondValidationError($message = "",$details=[]){		
		$message=($message=='')?trans('messages.parameters-fail-validation'):$message;
		return $this->setStatuscode(422)->respondWithError($message,$details);
	}
	public function respond($data,$headers=[]){
		return response($data,$this->getStatuscode())->withHeaders($headers);
	}

	public function respondWithError($message,$details=[]){
		return $this->respond([
			'status_code' =>$this->getStatuscode(),
			'message'     =>$message,
			'error'       =>['details'=>$details],
		]);
	}

	public function respondCreated($message){
		return $this->setStatuscode(201)->respond(['message'=>$message]);
	}

	protected function respondWithPagination($obj ,$data){		
		$data=array_merge($data,[
				"paginator"=>[
					'total_count'  =>$obj->total(),
					'total_pages'  => $obj->lastPage(),
					'current_page' => $obj->currentPage(),
					'limit'        => $obj->perPage()
				]
			]);
		return $this->respond($data);
		// return $this->respond($data);
	}

	public function last_query($db_Obj){
		$queries = \DB::connection($db_Obj)->getQueryLog();
		$last_query = end($queries);		
		return $last_query;
	}
}