<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apicategory;
use App\Transformers\CategoryTransformer;

class ApicategoryController extends ApiController
{

    protected $category;
    public function __construct(Apicategory $category,CategoryTransformer $categoryTransformer)
    {
        $this->category            =$category;
        $this->categoryTransformer =$categoryTransformer;
        // $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sort= request()->has('sort')?request()->get('sort'):'title';
        $order= request()->has('order')?request()->get('order'):'asc';
        $search= request()->has('searchQuery')?request()->get('searchQuery'):'';

        $categories=$this->category->where(function($query) use ($search)
        {
            if ($search) {
                $query->where('description','like',"$search%")
                    ->orWhere('title','like',"$search%");
            }
        })
        ->orderBy("$sort", "$order")->paginate(10);

        return $this->respondWithPagination($categories,[
            "status_code" =>$this->getStatuscode(),
            "message"     =>trans("messages.not-found"),
            "data"        =>$this->categoryTransformer->transformCollection($categories->all()),
        ]);       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
