<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apicategory;

class ApicategoriesController extends Controller
{
    protected $category;
	public function __construct(Apicategory $category)
	{
		$this->category=$category;
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category.list');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // VALIDATION OF INPUT
        $validator = validator()->make($request->all(), [
            'title'        =>'required|max:255',
            "description"  =>'required'
        ]);
        if ($validator->fails()) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        # Prepare input
        $input       =array_only($request->all(),["title","description"]);
        # Store
        $addedRecord =$this->category->create($input);
        # Response
        $response    =array_only($addedRecord->toArray(),['title','description','id']);
        # Respond in JSON
        if($request->wantsJson()){
            return response(["data"=>$response,"message"=>trans('messages.category-add'),"status_code"=>201],201);
        }
        flash(trans('messages.category-add'),'success');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = validator()->make($request->all(), [
            'title'        =>'required|max:255',
            "description"  =>'required'
        ]);
        if ($validator->fails()) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        $input=array_only($request->all(),["title","description"]);

        $category=$this->category->find($id);
        $updatedRecord=$category->update($input);
        return response(["data"=>$updatedRecord,"status_code"=>200],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=$this->category->find($id);
        $category->delete();
        return response(["data"=>[],"status_code"=>200],200);
    }
}
