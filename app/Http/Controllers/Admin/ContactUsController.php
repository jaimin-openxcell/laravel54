<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContactUs;

class ContactUsController extends Controller
{
	protected $contactUs;
    public function __construct(ContactUs $contactUs)
    {
    	$this->contactUs = $contactUs;
    }

    public function index(){
    	if (\Gate::denies('developerOnly') && \Gate::denies('contactUs.list')) {
            return back();
        }
        // If there is an Ajax request or any request wants json data
        if(request()->ajax() || request()->wantsJson()){
            $sort= request()->has('sort')?request()->get('sort'):'created_at';
            $order= request()->has('order')?request()->get('order'):'asc';
            $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
            $contactRequests=$this->contactUs->where(function($query) use ($search)
            {
                if ($search) {
                    $query->where('name','like',"$search%")
                        ->orWhere('description','like',"$search%")
                        ->orWhere('subject','like',"$search%")
                        ->orWhere('email','like',"$search%");
                }
            })
            ->orderBy("$sort", "$order")->paginate(10);

            if($contactRequests->count()<=0){
                return response([
                    "status_code" => 404,
                    "message"     => trans('messages.not-found')
                ],404);
            }
            $paginator=[
                'total_count'  =>$contactRequests->total(),
                'total_pages'  => $contactRequests->lastPage(),
                'current_page' => $contactRequests->currentPage(),
                'limit'        => $contactRequests->perPage()
            ];
            return response([
				"data"        =>$contactRequests->all(),
				"paginator"   =>$paginator,
				"status_code" =>200
            ],200);
        }
        return view('admin.contactUs.list');
    }

    public function switchStatus(Request $request){
        if (\Gate::denies('developerOnly') && \Gate::denies('transaction.status')) {
            return back();
        }
        $validator = validator()->make($request->all(), [
            'new_status' =>'required |in:completed,fail,cancelled,in-process',
            "id"         =>'required'
        ]);
        if ($validator->fails()) {
            return response(["error"=>trans('messages.parameters-fail-validation')],422);
        }
        extract($request->all());
        $transaction= $this->transaction->find($id);
        if($transaction){
            $transaction->status=$new_status;
            $transaction->save();
            // Get New updated Object of User
            $updated = $transaction;

            if($request->wantsJson()){
                return response([
                    "data"        =>$transaction->toArray(),
                    "message"     =>trans('messages.transaction-status',["status"=>$new_status]),
                    "status_code" =>200
                ],200);
            }
            flash(trans('messages.transaction-status',["status"=>$new_status]),'success');
            return back();
        }
        flash(trans('messages.transaction-update-fail'),'error');
        return back();
    }
}