<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apisdoc;
use App\Traits\CategoryTrait;
use App\Transformers\ApisTransformer;

class ApidocController extends Controller
{
    use CategoryTrait;
    protected $apidoc;
    protected $apisTransformer;

    public function __construct(Apisdoc $apidoc, ApisTransformer $apisTransformer)
    {
        $this->apidoc          =$apidoc;
        $this->apisTransformer =$apisTransformer;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.apidoc.index');
    }
    public function getAllAPIs(){
        // $apis=$this->apidoc->paginate(10);
        // mprd($apis->toArray());
        $sort= request()->has('sort')?request()->get('sort'):'title';
        $order= request()->has('order')?request()->get('order'):'asc';
        $search= request()->has('searchQuery')?request()->get('searchQuery'):'';
        $apis=$this->apidoc->where(function($query) use ($search)
        {
            if ($search) {
                $query->where('description','like',"$search%")
                    ->orWhere('title','like',"$search%");
            }
        })
        ->orderBy("$sort", "$order")->paginate(10);
        // Paginator For this list
        $paginator=[
            'total_count'  =>$apis->total(),
            'total_pages'  => $apis->lastPage(),
            'current_page' => $apis->currentPage(),
            'limit'        => $apis->perPage()
        ];
        return response([
            "data"=>$this->apisTransformer->transformCollection($apis->all()),
            "paginator"=>$paginator,"status_code"=>200
        ],200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=$this->fetchSliderOptions();
        return view("admin.apidoc.add",compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = validator()->make($request->all(), [
            'title'        =>'required|max:255',
            "description"  =>'required',
            'url'          =>'required',
            'request'      =>'required',
            'response'     =>'required',
            'request_type' =>'required',
            'category_id'  =>'required',
        ]);

        if ($validator->fails()) {
            flash(trans("messages.parameters-fail-validation"),'danger');
            return back()->withErrors($validator)->withInput();
        }
        $input=$request->all();
        $input['example']="example";
        $newApi=$this->apidoc->create(array_only($input,
            ["title", "description", "url", "request", "response", "request_type", "category_id","example"]));
        flash(trans('messages.api-add'),'success');
        return back()->with('show-alert',true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $apidoc=$this->apidoc->find($id);
        $apidoc->delete();
        return response(["data"=>[],"status_code"=>200],200);
    }
}
