<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $fillable = [
        "title","excerpt","body",'slug','meta_description','meta_keywords','author_id'
    ];
    public function author(){
    	return $this->belongsTo(Admin::class);
    }
}
