<?php 
use Illuminate\Contracts\Encryption\DecryptException;
use Carbon\Carbon;
function flash($message, $level='info')
{
	session()->flash('flash_message',$message);
	session()->flash('flash_message_level',$level);
}

function mpr($d, $echo = TRUE)
{
   	if($echo)
   	{
   		echo '<pre>'.print_r($d, true).'</pre>';
   	}
   	else
   	{
   		return '<pre>'.print_r($d, true).'</pre>';
    }
}

function mprd($d){
   mpr($d);
   die;
}

function mongoId($id){
  return new MongoId($id);
}

function mongoDateTimestamp($input){
  $utcdatetime = new MongoDB\BSON\UTCDateTime((string)$input);
  $datetime = $utcdatetime->toDateTime();
  return $datetime->format('U');
}

function mongoDate($input){
  $date = new MongoDB\BSON\UTCDateTime(strtotime($input));
  return $date;
}

function mongoDateStr($input){
  $date = new MongoDB\BSON\UTCDateTime($input * 1000);
  return $date;
}

function getTimestamp($date=''){
    if($date==''){
      return null;
    }
    if(is_string($date)){
      $d = new DateTime("$date");
      return $d->getTimestamp();
    }
    elseif(is_array($date)){
      return $date['sec'];
    }
    else{
      $className = get_class($date);
      switch ($className) {
        case 'Carbon\Carbon':
          return $date->timestamp;
          break;
        case 'MongoDB\BSON\UTCDateTime':
          return (integer)mongoDateTimestamp($date);
          break;
        default:
          # code...
          break;
      }
    }

}


function clean($string){
  $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
  return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function randomInteger($num=6){
  $value=1;
  $start=str_pad($value, $num, '0', STR_PAD_RIGHT);
  $end=($start*10) - 1;
  return mt_rand($start, $end);
}

function serializeEnc($array){
  return encrypt(serialize($array));
}

function serializeDec($array){
  return unserialize(decrypt($array));
}
function truncateDateToDay($time)
{
    $reset = date_default_timezone_get();
    date_default_timezone_set('UTC');
    $stamp = strtotime('today', $time);
    date_default_timezone_set($reset);
    return $stamp;
}