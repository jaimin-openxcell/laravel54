<?php 

namespace App\Traits;
use App\Device;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

trait NotificationsTrait{
    
    public function getDevices($userID){
        $devices = Device::with('user')->where("user_id",$userID)->first();
        if(!$devices){
            return collect();
        }
        return $devices;
    }

    /**
     * [fire description]
     * @param  [type] $user    [description]
     * @param  [type] $title   [description]
     * @param  [type] $message [description]
     * @param  array  $dataSet [description]
     * @return [type]          [description]
     */
    public function singlePushFire($user,$title,$message, $dataSet=[]){
        // You must change it to get your tokens
        $userDevice = $this->getDevices($user->id);
        if(! $userDevice->count()){
            return false;
        }
        $tokens     = $userDevice->devices;

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($message)->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($dataSet);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification);

        return true;
    }
    /**
     * [wineApprovedNotify description]
     * @param  [type] $user [description]
     * @param  [type] $wine [description]
     * @return [type]       [description]
     */
    public function wineApprovedNotify($user, $wine){
        $message = trans('app.NOTIFICATION_MATCHED_WINE');
        $this->singlePushFire($user,"WineMinder",$message,["wine_id" =>$wine->id ]);
    }
    /**
     * [wineRejectedNotify description]
     * @param  [type] $user [description]
     * @param  [type] $wine [description]
     * @return [type]       [description]
     */
    public function wineRejectedNotify($user, $wine){
        $message = trans('app.NOTIFICATION_REJECTE_WINE');
        $this->singlePushFire($user,"WineMinder",$message,["wine_id" =>$wine->id ]);
    }

    public function testNotify($user){
        $this->singlePushFire($user,"WineMinder","Test Message",["data" =>"This is dataset."]);
    }
}