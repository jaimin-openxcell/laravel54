<?php

namespace App\Transformers;
class ApisTransformer extends  Transformer
{
	public function transform($item){
		return [
			'id'          =>$item['id'],
			'title'       =>$item['title'],
			"description" =>$item['description']
		];
	}
}