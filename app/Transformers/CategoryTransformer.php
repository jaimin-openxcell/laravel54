<?php

namespace App\Transformers;
class CategoryTransformer extends  Transformer
{
	public function transform($item){
		return [
			'id'          =>$item['id'],
			'title'       =>$item['title'],
			"description" =>$item['description']
		];
	}
}