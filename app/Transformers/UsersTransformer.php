<?php

namespace App\Transformers;
class UsersTransformer extends  Transformer
{
	public function transform($item){
		return [
			'id'                    => $item['id'],
			'first_name'            => $item['first_name'],
			'last_name'             => $item['last_name'],
			'fullname'              => $item['first_name'].' '.$item['last_name'],
			"username"              => $item['username'],
			"email"                 => $item['email'],
			// "phone"              =>$item['phone'],
			// "verified"           =>$item['verified'],
			'status'                => $item['status'],
			'created_at'            => $item['created_at'],
			'notification' => $item['notification'],
			"api_token"             => $this->apiToken($item)
		];
	}

	protected function apiToken($item){
		return (isset($item['api_token']) && $item['api_token']!="")?$item['api_token']:'';
	}
}