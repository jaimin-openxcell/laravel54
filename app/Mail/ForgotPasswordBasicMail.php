<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPasswordBasicMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $pwd;
    public $level;
    public $introLines;
    public $outroLines;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user,$pwd)
    {
        $this->user  = $user;
        $this->pwd   =$pwd;
        $this->level = '';
        $this->introLines=["We were notified to send you OTP to reset your password."];
        $this->outroLines=[
            "Please use this in application to change your password.",
            "If this was sent on accident, no worries, just forget about it.",
            "If you are receiving this and didn't ask for it, we recommend that you change your password immediately",
            "for extra protection.",
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(config('app.name', 'Laravel').': Password reset request')->view('emails.forgot-password-basic');
    }
}
