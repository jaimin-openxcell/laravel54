<?php
/*DB::listen(function($query){
	Log::info($query->sql, $query->bindings);
});*/
Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();

    //dd($users);

    return view('admin.home');
})->name('home');
/**
 * CATEGORY
 */
/*Route::get('/apicategory/all',"Admin\ApicategoriesController@datatable");
Route::resource('/apicategory',"Admin\ApicategoriesController");*/

/**
 * ROLES
 */
Route::get('/role/{role}/permissions',"Admin\RoleController@permissions");
Route::get('/rolePermissions',"Admin\RoleController@rolePermissions")->name('myrolepermission');
Route::get('/roles/all',"Admin\RoleController@all");
Route::post('/assignPermission','Admin\RoleController@attachPermission');
Route::post('/detachPermission','Admin\RoleController@detachPermission');
Route::resource('/roles',"Admin\RoleController");

/**
 * PERMISSIONs
 */
Route::get('/permissions/all',"Admin\PermissionController@all");
Route::resource('/permissions',"Admin\PermissionController");

/**
 * API DOC
 */
/*Route::get('/apidoc/all',"Admin\ApidocController@getAllAPIs");
Route::resource('/apidoc',"Admin\ApidocController");*/

/**
 * ADMINs
 */
Route::get('/profile',"Admin\AdminController@profileEdit");
Route::put('/profile/{admin}',"Admin\AdminController@profileUpdate");
Route::put('/changepassword/{admin}',"Admin\AdminController@changePassword");
Route::put('/administrator/status',"Admin\AdminController@switchStatus");
Route::post('/administrator/removeBulk',"Admin\AdminController@destroyBulk");
Route::put('/administrator/statusBulk',"Admin\AdminController@switchStatusBulk");
Route::resource('/administrator',"Admin\AdminController");

/**
 * USERS
 */
Route::put('/user/status',"Admin\UserController@switchStatus");
Route::post('/user/removeBulk',"Admin\UserController@destroyBulk");
Route::put('/user/statusBulk',"Admin\UserController@switchStatusBulk");
Route::get('/user/{id}/cellar',"Admin\UserController@showUserCellar");
Route::resource('/user',"Admin\UserController");

/**
 * Contact US
 */
Route::resource('/contactUs',"Admin\ContactUsController");